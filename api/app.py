from textwrap import indent
from flask import Flask, render_template, request, redirect, url_for, jsonify, flash
import pandas as pd
from flask_wtf import FlaskForm, RecaptchaField
from flask_wtf.file import FileField, FileRequired, FileAllowed
from wtforms import SubmitField, TextField, StringField, PasswordField, SelectField, DateField
from wtforms.validators import DataRequired, Email, Length, EqualTo, URL
from faker import Faker
import json
from datetime import datetime,date
import os
d = os.path.dirname(os.getcwd())
faker = Faker()

app = Flask(__name__)
app.config['MAX_CONTENT_LENGTH'] = 1024 * 1024
app.config['UPLOAD_EXTENSIONS'] = ['.jpg', '.png', '.gif', '.csv']
app.config['UPLOAD_PATH'] = 'uploads'


def datetime_to_isoformat(obj):
    if isinstance(obj, (datetime, date)):
        return obj.isoformat()
    raise TypeError("Type %s is not serializable" % type(obj))

@app.route('/')
def index():
    """_summary_

    :return: _description_
    :rtype: _type_
    """
    return render_template('upload.html')


@app.route('/uploader', methods=['POST'])
def upload_file():
    """_summary_

    :return: _description_
    :rtype: _type_
    """
    uploaded_file = request.files['file']
    if uploaded_file.filename != '':
        df = pd.read_csv(uploaded_file, sep=",")
        return jsonify(body=df.to_json(orient="records"))
    return redirect(url_for('upload'))


@app.route('/users/<int:number>')
def users(number: int):
    """_summary_

    :param number: _description_
    :type number: int
    :return: _description_
    :rtype: _type_
    """
    data = [faker.simple_profile() for _ in range(number)]
    return jsonify(message=data)


@app.route('/generate_users/<int:number>')
def generate_users(number: int):
    """
    """
    data = [faker.simple_profile() for _ in range(number)]
    now = datetime.now()
    file_ts = now.strftime(format="%Y%m%d%H%M%S")
    data_dir = f"{os.path.dirname(os.path.realpath(__file__))}/data"
    with open(f"{data_dir}/users_{file_ts}.csv", "w") as users:
        json.dump(data, users, ensure_ascii=False, sort_keys=True,
                  indent=4, default=datetime_to_isoformat)
    return jsonify(message=data)

if __name__=="__main__":
    app.run(debug=True)
